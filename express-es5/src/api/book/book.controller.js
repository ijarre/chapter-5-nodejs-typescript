const axios = require('axios')
const response = require('../../../helper/response')
class BookController {
    async getAllBooks(req, res) {
        try {
            axios.get(`${process.env.BOOK_URL}/book/list?key=${process.env.BOOK_KEY}`)
                .then(function (response) {
                    return response.data
                })
                .catch(function (error) {
                    console.log(error)
                    response.errorInternal(res)
                })
                .then(function (data) {
                    response.success(res, 200, data.meta, data.data, "Success Get All Books.")
                });
        } catch (error) {
            console.log(error)
            response.errorInternal(res)
        }
    }
}

module.exports = BookController