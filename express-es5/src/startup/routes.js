const express = require('express')
const apiRouter = require('../api')

const router = express.Router()


router.use('/v1/book', apiRouter.bookRouter)

module.exports = router
